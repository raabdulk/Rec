
#include <iostream>
#include <cmath>
#include <map>
#include <sstream>
#include <algorithm>

// local
#include "RichHPDImageAnalysis/SCurveNormalisedHPDImage.h"

using namespace Rich::HPDImage;

//-----------------------------------------------------------------------------
// Implementation file for class : SCurveNormalised
//
// 2011-03-07 : Chris Jones
//-----------------------------------------------------------------------------

std::shared_ptr<TH2D> SCurveNormalised::filter() const
{

  // Compute the average bin content normalisation factor
  double totEn(0);
  unsigned int nBins(0);
  for ( int i = 0; i < m_inHist->GetNbinsX(); ++i )
  {
    for ( int  j = 0; j < m_inHist->GetNbinsY(); ++j )
    {
      ++nBins;
      totEn += m_inHist->GetBinContent(i+1,j+1);
    }
  }
  const auto normF = 1.0 * ( nBins > 0 ? totEn/nBins : 0.0 );
  
  // construct unique ID each time ...
  // nonsense to keep ROOT happy
  static unsigned long long iH(0);
  std::ostringstream id;
  id << m_inHist->GetName() << "_SCurveNorm_" << ++iH;
  
  // make a new histogram
  std::shared_ptr<TH2D> lzH  
    ( new TH2D ( id.str().c_str(),
                 (std::string(m_inHist->GetTitle())+" | SCurveNorm").c_str(),
                 m_inHist->GetNbinsX(),
                 m_inHist->GetXaxis()->GetXmin(),
                 m_inHist->GetXaxis()->GetXmax(),
                 m_inHist->GetNbinsY(),
                 m_inHist->GetYaxis()->GetXmin(),
                 m_inHist->GetYaxis()->GetXmax() ) 
      );
  lzH->GetXaxis()->SetTitle( m_inHist->GetXaxis()->GetTitle() );
  lzH->GetYaxis()->SetTitle( m_inHist->GetYaxis()->GetTitle() );
  
  // loop over bins and fill
  for ( int i = 0; i < m_inHist->GetNbinsX(); ++i )
  {
    for ( int  j = 0; j < m_inHist->GetNbinsY(); ++j )
    {
      // raw bin content
      const auto rawCont  = m_inHist->GetBinContent(i+1,j+1);
      // normalise it
      const auto normCont = ( normF>0.0 ? normF*std::tanh(rawCont/normF) : 0 );
      // fill
      lzH->Fill( i, j, normCont );
    }
  }
  
  // return
  return lzH;
}
